(async function() {
    function getVerifier(proofs, proofUrl, fingerprint) {
        for (const proof of proofs) {
            const matches = proofUrl.match(new RegExp(proof.matcher));
            if (!matches) continue;

            const bound = Object.entries(proof.variables).map(([key, value]) => [key, matches[value || 0]]).reduce((previous, current) => { previous[current[0]] = current[1]; return previous;}, { FINGERPRINT: fingerprint });

            const profile = proof.profile.replace(/\{([A-Z]+)\}/g, (_, name) => bound[name]);

            const proofJson = proof.proof.replace(/\{([A-Z]+)\}/g, (_, name) => bound[name]);

            const username = proof.username.replace(/\{([A-Z]+)\}/g, (_, name) => bound[name]);

            return {
                profile,
                proofUrl,
                proofJson,
                username,
                service: proof.service,
                checks: (proof.checks || []).map(check => ({
                    relation: check.relation,
                    proof: check.proof,
                    claim: check.claim.replace(/\{([A-Z]+)\}/g, (_, name) => bound[name])
                }))
            };
        }
        return null;
    }

    const keyUrl = document.querySelector('[rel="pgpkey"]').href;
    const response = await fetch(keyUrl);
    const armor = await response.text();
    const key = (await openpgp.key.readArmored(armor)).keys[0];

    const fingerprint = key.primaryKey.getFingerprint();

    const primaryUser = await key.getPrimaryUser();
    const profileHash = await openpgp.crypto.hash.md5(openpgp.util.str_to_Uint8Array(primaryUser.user.userId.email)).then((u) => openpgp.util.str_to_hex(openpgp.util.Uint8Array_to_str(u)));
    // there is index property on primaryUser
    document.title = primaryUser.user.userId.name + ' - OpenPGP key';
    const name = primaryUser.user.userId.name;

    document.querySelector('.avatar').src = "https://seccdn.libravatar.org/avatar/" + profileHash + "?s=148&d=" + encodeURIComponent("https://www.gravatar.com/avatar/" + profileHash + "?s=148&d=mm");
    document.querySelector('.name').textContent = name;

    const p = (await (await fetch('proofs.json')).json()).proofs;
    const proofUrls = new Set();

    for (const user of key.users) {
        try {
            const valid = await user.verify(key.primaryKey);
            if (valid) {
                const notations = user.selfCertifications[0].notations || [];
                notations
                    .filter(notation => notation[0] === 'proof@metacode.biz' && typeof notation[1] === 'string')
                    .map(notation => notation[1])
                    .forEach(proofUrls.add.bind(proofUrls));
            }
        } catch (e) {
            console.error('User verification error:', e);
        }
    }

    [...proofUrls]
        .sort()
        .map(proofUrl => getVerifier(p, proofUrl, fingerprint))
        .filter(Boolean)
        .map(proof => {
            const li = document.createElement('li');
            const profile = document.createElement('a');
            profile.rel = 'me noopener nofollow';
            profile.target = '_blank';
            profile.href = proof.profile;
            profile.textContent = proof.service + ': ' + proof.username;
            li.appendChild(profile);
            const verification = document.createElement('a');
            verification.className = 'proof';
            verification.rel = 'me noopener nofollow';
            verification.target = '_blank';
            verification.href = proof.proofUrl;
            verification.dataset.proofJson = proof.proofJson;
            verification.dataset.checks = JSON.stringify(proof.checks);
            verification.textContent = 'proof';
            li.appendChild(verification);
            return li;
        }).forEach(document.getElementById('proofs').appendChild.bind(document.getElementById('proofs')));

    async function verify(json, checks) {
        for (const check of checks) {
            const proofValue = check.proof.reduce((previous, current) => {
                if (current == null || previous == null) return null;
                if (Array.isArray(previous) && typeof current === 'string') {
                    return previous.map(value => value[current]);
                }
                return previous[current];
            }, json);
            const claimValue = check.claim;
            if (check.relation === 'eq') {
                if (proofValue !== claimValue) {
                    throw new Error(`Proof value ${proofValue} !== claim value ${claimValue}`);
                }
            } else if (check.relation === 'contains') {
                if (!proofValue || proofValue.indexOf(claimValue) === -1) {
                    throw new Error(`Proof value ${proofValue} does not contain claim value ${claimValue}`);
                }
            } else if (check.relation === 'oneOf') {
                if (!proofValue || proofValue.indexOf(claimValue) === -1) {
                    throw new Error(`Proof value ${proofValue} does not contain claim value ${claimValue}`);
                }
            }
        }
    }

    async function checkProofs() {
        const proofs = document.querySelectorAll('[data-checks]');
        for (const proofLink of proofs) {
            const checks = JSON.parse(proofLink.dataset.checks || '');
            const url = proofLink.dataset.proofJson || '';
            try {
                await verify(await (await fetch(url)).json(), checks);
                proofLink.textContent = 'verified';
                proofLink.classList.add('verified');
            } catch(e) {
                console.error('Could not verify proof: ' + e);
            }
        }
    }

    checkProofs();

}());